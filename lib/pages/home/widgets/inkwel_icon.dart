import 'package:digikala/configuration/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: camel_case_types
class InkwellButton extends StatefulWidget {
  final icon;
  final onTap;

  const InkwellButton({this.icon, this.onTap});

  @override
  _InkwellButtonState createState() => _InkwellButtonState();
}

class _InkwellButtonState extends State<InkwellButton> {
  bool tap = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      width: 48,
      margin: EdgeInsets.only(left: 4),
      child: Material(
        color: Colors.transparent,
        child: InkWell(

          onHighlightChanged: (event){
            print(event);
            if(event)
              setState(() {
                tap = true;
              });
            else
              setState(() {
                tap = false;
              });
          },

          splashColor: Colors.white.withOpacity(.1),
          highlightColor: Colors.white.withOpacity(.1),
          customBorder: ShapeBorder.lerp(
              RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white, width: 1)),
              RoundedRectangleBorder(
                  side: BorderSide(color: Colors.white, width: 1)),
              1),
          onTap: widget.onTap,
          child: Icon(
            widget.icon,
            color: tap 
            ?MyColors.fontColor.withOpacity(.18)
            :MyColors.fontColor,
          ),
        ),
      ),
    );
  }
}
