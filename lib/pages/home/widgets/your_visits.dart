import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/home/widgets/show_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


List _list = [
  "https://dkstatics-public.digikala.com/digikala-products/203131.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
  "https://dkstatics-public.digikala.com/digikala-products/114389900.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
  "https://dkstatics-public.digikala.com/digikala-products/112810891.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
  "https://dkstatics-public.digikala.com/digikala-products/112796789.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
  "https://dkstatics-public.digikala.com/digikala-products/112810595.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
  "https://dkstatics-public.digikala.com/digikala-products/114704427.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
  "https://dkstatics-public.digikala.com/digikala-products/111721871.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",  "https://dkstatics-public.digikala.com/digikala-products/114389900.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
  "https://dkstatics-public.digikala.com/digikala-products/112810891.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
];

class YourVisits extends StatelessWidget {

  bottomBorders (int index){
    if(index<6){
      return BorderSide(width: .2,color: MyColors.fontColor.withOpacity(.56));
    }
    else
      return BorderSide(width: 0,color: MyColors.iconColor);
  }
  rightBorders(int index) {
    if(index%3 != 0){
      return BorderSide(width: .2,color: MyColors.fontColor.withOpacity(.56));
    }
    else
      return BorderSide(width: 0,color: MyColors.iconColor);
  }

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      color: MyColors.background,
      height: screenWidth,
      padding: EdgeInsets.symmetric(horizontal: 16),
      width: screenWidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text(Strings.yourVisitsOne,style: TextStyle(
              fontWeight: FontWeight.bold,color: MyColors.blackColor.withOpacity(.87),
              fontSize: 16
          ),),
          const SizedBox(height: 4),
          Text(Strings.sortBy,style: TextStyle(
              color: MyColors.fontColor,
              fontSize: 14
          ),),
          const SizedBox(height: 8),
          GridView.builder(
              itemCount: _list.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: 16),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  childAspectRatio: 1/.9),
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () => showProduct(context: context,img: _list[index]),
                  child: Container(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: bottomBorders(index),
                            right: rightBorders(index)
                        )
                    ),
                    child: Image.network(
                      _list[index],
                      fit: BoxFit.contain,
                    ),
                  ),
                );
              }),
          const SizedBox(height: 22),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(Strings.seenAllVisits,style: TextStyle(
                  color: MyColors.primary,fontWeight: FontWeight.bold
              ),),
              const SizedBox(width: 8),
              Icon(Icons.arrow_forward_ios,color: MyColors.primary,size: 16,)
            ],
          )
        ],
      ),
    );
  }


}
