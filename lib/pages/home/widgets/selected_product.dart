import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/home/model/selected_product_model.dart';
import 'package:digikala/pages/home/widgets/show_product.dart';
import 'package:flutter/material.dart';

List<SelectedProductModel> _list = [
  SelectedProductModel(
      title:
          "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/203131.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
  SelectedProductModel(
      title:
          "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/113846203.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
  SelectedProductModel(
      title:
          "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/203131.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
  SelectedProductModel(
      title:
      "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114704427.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
  SelectedProductModel(
      title:
          "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
          "https://dkstatics-public-2.digikala.com/digikala-products/110929962.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
  SelectedProductModel(
      title:
          "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
  SelectedProductModel(
      title:
          "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
          "https://dkstatics-public-2.digikala.com/digikala-products/110491661.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
  SelectedProductModel(
      title:
          "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/115099091.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
  SelectedProductModel(
      title:
          "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114704427.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
  SelectedProductModel(
      title:
          "گوشی تلفن همراه شیایومی مدل 10x80 سال 2020 دو سیم کارت همزمان فعال",
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/112145268.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      offer: "31%",
      cost: "326.000تومان",
      offerCost: "300.000"),
];

class SelectedProduct extends StatelessWidget {
  bottomBorders(int index) {
    if (index < _list.length - 2) {
      return BorderSide(width: .2, color: MyColors.fontColor.withOpacity(.56));
    } else
      return BorderSide(width: 0, color: MyColors.iconColor);
  }

  leftBorders(int index) {
    if (index % 2 != 0) {
      return BorderSide(width: 0, color: MyColors.iconColor);
    } else
      return BorderSide(width: .2, color: MyColors.fontColor.withOpacity(.56));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            Strings.selectedProductTitle,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                color: MyColors.blackColor.withOpacity(.87),
                fontSize: 16),
          ),
        ),
        const SizedBox(height: 24),
        Container(
          width: MediaQuery.of(context).size.width,
          child: GridView.builder(
              itemCount: _list.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: 16),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, childAspectRatio: 10 / 16),
              itemBuilder: (context, index) {
                final items = _list[index];
                return GestureDetector(
                  onTap: () => showProduct(context: context,img: items.imgUrl),
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                        border: Border(
                      bottom: bottomBorders(index),
                      left: leftBorders(index),
                    )),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        const SizedBox(height: 8),
                        ClipRRect(
                          borderRadius: BorderRadius.all(Radius.circular(4)),
                          child: Image.network(
                            items.imgUrl,
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        const SizedBox(height: 8),
                        Text(
                          items.title.length < 43
                              ? items.title
                              : items.title.substring(0, 42) + "...",
                          style: TextStyle(
                            fontSize: 12,
                            color: MyColors.fontColor,
                          ),
                        ),
                        const SizedBox(height: 32),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Expanded(
                              flex: 3,
                              child: Container(
                                width: 46,
                                height: 18,
                                decoration: BoxDecoration(
                                    color: MyColors.primary,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(26))),
                                child: Center(
                                  child: Text(
                                    items.offer,
                                    style: TextStyle(
                                        color: MyColors.iconColor, fontSize: 10),
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                                flex: 8,
                                child: Column(
                                  children: <Widget>[
                                    const SizedBox(height: 3),
                                    Text(
                                      items.cost,
                                      style: TextStyle(
                                          color: MyColors.blackColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                      textAlign: TextAlign.end,
                                    ),
                                    const SizedBox(height: 4),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 16),
                                      child: Text(
                                        items.offerCost,
                                        style: TextStyle(
                                            decoration:
                                                TextDecoration.lineThrough,
                                            color: MyColors.fontColor,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 10),
                                        textAlign: TextAlign.end,
                                      ),
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              }),
        ),
      ],
    );
  }
}
