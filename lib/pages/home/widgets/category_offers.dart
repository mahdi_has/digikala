import 'package:digikala/configuration/links.dart';
import 'package:flutter/material.dart';

List<String> _list = [
 Links.catOfferOne,
  Links.catOfferTwo,
  Links.catOfferThree,
  Links.catOfferFour
];

class CategoryOffers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return LayoutBuilder(
      builder: (context , screenSize){
        return Container(
          width: screenWidth,
          height: screenSize.biggest.width >400 ? 220 : 190,
          child: GridView.builder(
              itemCount: _list.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              padding: EdgeInsets.symmetric(horizontal: 16),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 8,
                  childAspectRatio: 15.5 / 9),
              itemBuilder: (context, index) {
                return ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                  child: Image.network(
                    _list[index],
                    fit: BoxFit.fitWidth,
                  ),
                );
              }),
        );
      },

    );
  }
}
