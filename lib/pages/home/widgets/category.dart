import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/home/model/icon_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

List<IconModel> _list = [
  IconModel(
      icon: Entypo.clock,
      text: Strings.categoryTxtOne,
      color: MyColors.primary),
  IconModel(
      icon: Entypo.shopping_basket,
      text: Strings.categoryTxtTwo,
      color: MyColors.catTwo),
  IconModel(
      icon: Entypo.awareness_ribbon,
      text: Strings.categoryTxtThree,
      color: MyColors.catThree),
  IconModel(
      icon: Icons.computer,
      text: Strings.categoryTxtFour,
      color: MyColors.catFour),
  IconModel(
    icon: Icons.beach_access,
    text: Strings.categoryTxtFive,
    color: MyColors.catFive
  )
];

class Category extends StatefulWidget {
  @override
  _CategoryState createState() => _CategoryState();
}

class _CategoryState extends State<Category> {
  List<bool> tap = [false,false,false,false,false];
  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;

    return Container(
      width: screenWidth,
      child: GridView.builder(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 5,
          ),
          padding: EdgeInsets.symmetric(horizontal: 16),
          scrollDirection: Axis.vertical,
          itemCount: _list.length,
          itemBuilder: (context, item) {
            final items = _list[item];
            return InkWell(
              onHighlightChanged: (event){
                print(event);
                if(event)
                  setState(() {
                    tap[item] = true;
                  });
                else
                  setState(() {
                    tap[item] = false;
                  });
              },
              onTap: (){},
              splashColor: Colors.white.withOpacity(.1),
              highlightColor: Colors.white.withOpacity(.1),
              customBorder: ShapeBorder.lerp(
                  RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white, width: 1)),
                  RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white, width: 1)),
                  1),
              child: Stack(
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        height: 48,
                        width: 48,
                        margin: EdgeInsets.only(bottom: 4),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: items.color,
                        ),
                        child: Icon(items.icon, size: 30, color: MyColors.iconColor),
                      ),
                      const SizedBox(
                        height: 4
                      ),
                      Text(
                        items.text,
                        style: TextStyle(fontSize: 9, color: MyColors.fontColor),
                      )
                    ],
                  ),
                  Visibility(
                    visible: tap[item],
                    child: Container(
                      width: 52,
                      height: 66,
                      color: MyColors.itemsColor.withOpacity(.38),

                    ),
                  ),
                ],
              ),
            );
          }),
    );
  }
}
