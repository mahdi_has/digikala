import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/home/widgets/page_view_items.dart';
import 'package:flutter/material.dart';

class BestSellingGoods extends StatelessWidget {
  final _pageController =
      PageController(viewportFraction: .8, initialPage: 0, keepPage: false);

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      width: screenWidth,
      height: 344,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 16, top: 16, bottom: 16),
            child: Text(
              Strings.bestSelling,
              style: TextStyle(
                  color: MyColors.blackColor.withOpacity(.87),
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
            ),
          ),
          Container(
            width: screenWidth,
            height: 288,
            child: PageView(controller: _pageController, children: [
              Container(
                width: screenWidth,
                height: 288,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    PageViewItems(index: 1,),
                    PageViewItems(index: 2,),
                    PageViewItems(index: 3,),
                  ],
                ),
              ),
              Container(
                width: screenWidth,
                height: 288,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    PageViewItems(index: 4,),
                    PageViewItems(index: 5,),
                    PageViewItems(index: 6,),
                  ],
                ),
              ),
              Container(
                width: screenWidth,
                height: 288,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    PageViewItems(index: 7,),
                    PageViewItems(index: 8,),
                    PageViewItems(index: 9,),
                  ],
                ),
              ),
              Container(
                width: screenWidth,
                height: 288,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    PageViewItems(index: 10,),
                    PageViewItems(index: 11,),
                    PageViewItems(index: 12,),
                  ],
                ),
              ),
            ]),
          ),
        ],
      ),
    );
  }
}
