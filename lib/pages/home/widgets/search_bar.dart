import 'package:digikala/configuration/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

SearchBar() {
  return PreferredSize(
    preferredSize: Size.fromHeight(65),
    child: SafeArea(
      child: Material(
        elevation: 4,
        child: Container(
          margin: EdgeInsets.all(8),
          height: 56,
          decoration: BoxDecoration(
            color: MyColors.baseColor,
            borderRadius: BorderRadius.all(Radius.circular(4)),
          ),
          child: Flex(
            direction: Axis.horizontal,
            children: <Widget>[
              const SizedBox(width: 4),
              Expanded(
                flex: 1,
                child: GestureDetector(
                  child: Icon(Feather.search),
                ),
              ),
              Expanded(
                flex: 9,
                child: Row(
                  children: <Widget>[
                    const SizedBox(width: 18),
                    Text(
                      "جستجو در ",
                      style: TextStyle(
                          fontSize: 18,
                          color: MyColors.fontColor,
                          fontStyle: FontStyle.normal),
                    ),
                    Text(
                      "دیـجـیـ",
                      style: TextStyle(
                          color: MyColors.fontColor,
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    ),
                    Text(
                      " کالــا",
                      style: TextStyle(
                          color: MyColors.primary,
                          fontSize: 22,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ),
  );
}
