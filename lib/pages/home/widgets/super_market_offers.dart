import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/images.dart';
import 'package:digikala/configuration/links.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/home/model/offers_model.dart';
import 'package:digikala/pages/home/widgets/show_product.dart';
import 'package:flutter/material.dart';

List<OffersModel> _list = [
  OffersModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "هدفون سامسونگ",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "هدفون سامسونگ",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/115081009.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "هدفون سامسونگ",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/112195068.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "هدفون سامسونگ",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/4079867.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "هدفون سامسونگ",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/1435840.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "هدفون سامسونگ",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/115099091.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "هدفون سامسونگ",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "هدفون سامسونگ",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "هدفون سامسونگ",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
];

class SuperMarketOffers extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    return Container(
      width: screenWidth,
      height: 374,
      color: MyColors.catTwo,
      child: ListView.builder(
          padding: EdgeInsets.symmetric(vertical: 24),
          scrollDirection: Axis.horizontal,
          itemCount: _list.length,
          itemBuilder: (context, index) {
            final items = _list[index];
            if (index == 0) {
              return Container(
                margin: EdgeInsets.only(left: 8),
                width: 170,
                height: 280,
                child: Padding(
                  padding: const EdgeInsets.only(right: 32),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      const SizedBox(height: 16),
                      const SizedBox(height: 8),
                      Image.network(
                        Links.supermarketOffer,
                        height: 240,
                      ),
                      const SizedBox(height: 36),
                      Row(
                        children: <Widget>[
                          Text(
                            Strings.seeAll,
                            style: TextStyle(
                                fontSize: 16, color: MyColors.iconColor),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: MyColors.iconColor,
                            size: 14,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              );
            }
            if (index == _list.length - 1) {
              return Container(
                  width: 170,
                  height: 260,
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  decoration: BoxDecoration(
                      color: MyColors.itemsColor,
                      borderRadius: BorderRadius.all(Radius.circular(6))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 48,
                        height: 48,
                        decoration: BoxDecoration(
                            border:
                                Border.all(color: MyColors.primary, width: 2),
                            shape: BoxShape.circle),
                        child: Icon(Icons.arrow_forward,
                            color: MyColors.primary, size: 32),
                      ),
                      const SizedBox(height: 8),
                      Text(
                        Strings.seeAll,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                            color: MyColors.fontColor),
                      )
                    ],
                  ));
            }
            return GestureDetector(
              onTap: () => showProduct(context: context,img: items.imgUrl),
              child: Container(
                  width: 174,
                  height: 344,
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  decoration: BoxDecoration(
                      color: MyColors.itemsColor,
                      borderRadius: BorderRadius.all(Radius.circular(6))),
                  child: Stack(
                    children: <Widget>[
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          const SizedBox(height: 16),
                          Image.network(
                            items.imgUrl,
                            height: 106,
                            width: 106,
                            fit: BoxFit.cover,
                          ),
                          Container(
                            height: 84,
                            padding: const EdgeInsets.symmetric(
                                horizontal: 16, vertical: 16),
                            child: Text(
                              items.text,
                              textAlign: TextAlign.start,
                              textDirection: TextDirection.rtl,
                              style: TextStyle(
                                  color: MyColors.fontColor,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 16, left: 16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Image.asset(
                                  Images.send,
                                  height: 32,
                                  width: 32,
                                ),
                                Text(
                                  Strings.send,
                                  style: TextStyle(fontSize: 11),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16),
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 3,
                                  child: Container(
                                    width: 50,
                                    height: 20,
                                    decoration: BoxDecoration(
                                        color: MyColors.primary,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(26))),
                                    child: Center(
                                      child: Text(
                                        items.offer,
                                        style: TextStyle(
                                            color: MyColors.iconColor,
                                            fontSize: 11),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                    flex: 8,
                                    child: Text(
                                      items.cost,
                                      style: TextStyle(
                                          color: MyColors.blackColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14),
                                      textAlign: TextAlign.end,
                                    ))
                              ],
                            ),
                          ),
                        ],
                      ),
                      Positioned(
                        top: 96,
                        right: 18,
                        child: Container(
                          width: 36,
                          height: 36,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border:
                                Border.all(color: MyColors.primary, width: 1.5),
                          ),
                          child: Icon(
                            Icons.add,
                            color: MyColors.primary,
                            size: 28,
                          ),
                        ),
                      ),
                      Positioned(
                        left: 46,
                        bottom: 50,
                        child: Text(
                          items.orginalCost != null
                              ? items.orginalCost
                              : "ناموجود",
                          style: TextStyle(
                              decoration: TextDecoration.lineThrough,
                              color: MyColors.fontColor),
                        ),
                      ),
                      Positioned(
                        bottom: 16,
                        left: 16,
                        child: Text(
                          items.time,
                          style: TextStyle(color: MyColors.fontColor),
                          textAlign: TextAlign.end,
                          textDirection: TextDirection.ltr,
                        ),
                      )
                    ],
                  )),
            );
          }),
    );
  }
}
