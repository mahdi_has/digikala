import 'package:digikala/configuration/colors.dart';
import 'package:digikala/pages/home/model/best_sellling_model.dart';
import 'package:digikala/pages/home/widgets/show_product.dart';
import 'package:flutter/material.dart';

List<BestSellingModel> _list = [
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/115598446.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/115598446.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/113562469.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/113846203.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/112145268.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114612944.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114533958.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114395955.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/114145010.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/113846203.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/115598446.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
          "https://dkstatics-public.digikala.com/digikala-products/113562469.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114533958.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
];

List<BestSellingModel> _dsiplayedList = [
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/111059101.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/115598446.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/113562469.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/97030.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/112145268.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114612944.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114395955.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/110744540.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/4079867.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public-2.digikala.com/digikala-products/110929962.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
  BestSellingModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114533958.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      title: "گوشی موبایل هواووی مدل y9سال 2019 دو سیم کارت "),
];
class PageViewItems extends StatelessWidget {
  final int index;
  final int type;
  const PageViewItems({Key key, this.index,this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () => showProduct(context: context,img: type == 2
          ?_dsiplayedList[index].imgUrl
          :_list[index].imgUrl,

      ),
      child: Container(
        width: screenWidth,
        height: 96,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.network(
              type == 2
              ? _dsiplayedList[index].imgUrl
              :_list[index].imgUrl,
              width: 84,
              height: 84,
              fit: BoxFit.fitWidth,
            ),
            const SizedBox(
              width: 4
            ),
            Text(
              "$index",
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold,color: MyColors.catFour),
            ),
            const SizedBox(
                width: 8
            ),
            Flexible(
              child: Container(
                padding: EdgeInsets.only(left: 16),
                  child: Text(
                    type == 2
                        ? _dsiplayedList[index].title
                        :_list[index].title,
                overflow: TextOverflow.clip,
                style: TextStyle(),
              )),
            )
          ],
        ),
      ),
    );
  }
}
