import 'package:carousel_slider/carousel_slider.dart';
import 'package:digikala/configuration/links.dart';
import 'package:flutter/cupertino.dart';

List<String> _bannerImg = [
  Links.banner1,
  Links.banner2,
  Links.banner3,
  Links.banner4,
  Links.banner5
];
final Duration _autoPlayDuration = const Duration(seconds: 3);
final Duration _pausePlayDuration = const Duration(seconds: 6);

// ignore: non_constant_identifier_names
Banners() {
  return StatefulBuilder(
    builder: (context, StateSetter setState) {
      return CarouselSlider(
          autoPlayAnimationDuration: _autoPlayDuration,
          viewportFraction: 0.8,
          aspectRatio: 15.5 / 6,
          autoPlay: true,
          pauseAutoPlayOnTouch: _pausePlayDuration,
          items: _bannerImg.map((banner) {
            return ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              child: Image.network(
                banner,
              ),
            );
          }).toList());
    },
  );
}
