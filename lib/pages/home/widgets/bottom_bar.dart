import 'package:digikala/configuration/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: non_constant_identifier_names
BottomBar(controller) {
  return BottomAppBar(
    elevation: 10,
    child: Container(
      height: 48,
      child: TabBar(
        tabs: <Widget>[
          Tab(
            child: GestureDetector(
              onTap: () {},
              child: Icon(
                Icons.home,
                color: MyColors.blackColor.withOpacity(.87),
              ),
            ),
          ),
          Tab(
            child: GestureDetector(
              onTap: () {},
              child: Icon(
                Icons.search,
                color: MyColors.fontColor,
              ),
            ),
          ),
          Tab(
            child: GestureDetector(
              onTap: () {},
              child: Icon(
                Icons.add_shopping_cart,
                color: MyColors.fontColor,
              ),
            ),
          ),

          Tab(
            child: GestureDetector(
              onTap: () {},
              child: Icon(
                Icons.person_outline,
                color: MyColors.fontColor,
              ),
            ),
          ),
        ],
        controller: controller,
      ),
    ),
  );
}
