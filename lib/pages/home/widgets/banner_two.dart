import 'package:digikala/configuration/links.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class BannerTwo extends StatelessWidget {
  String src = Links.advertisementBannerTwo;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(8)),
        child: Image.network(src),
      ),
    );
  }
}
