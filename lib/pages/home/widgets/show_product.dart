

import 'package:digikala/pages/show_product/product_detail.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

showProduct({BuildContext context , String img}){
  return Navigator.push(context, MaterialPageRoute(
    builder: (context) => ProductDetail(imgUrl: img,)
  ));
}