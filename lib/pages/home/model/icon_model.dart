

// ignore: non_constant_identifier_names
import 'package:flutter/material.dart';

class IconModel  {
  final icon, text, color;

  IconModel({this.icon, this.text, this.color});

}
