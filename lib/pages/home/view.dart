import 'package:digikala/configuration/colors.dart';
import 'package:digikala/pages/home/widgets/banner_one.dart';
import 'package:digikala/pages/home/widgets/banner_three.dart';
import 'package:digikala/pages/home/widgets/banner_two.dart';
import 'package:digikala/pages/home/widgets/banners.dart';
import 'package:digikala/pages/home/widgets/best_products_displayed.dart';
import 'package:digikala/pages/home/widgets/best_selling_goods.dart';
import 'package:digikala/pages/home/widgets/bottom_bar.dart';
import 'package:digikala/pages/home/widgets/category.dart';
import 'package:digikala/pages/home/widgets/category_offers.dart';
import 'package:digikala/pages/home/widgets/search_bar.dart';
import 'package:digikala/pages/home/widgets/selected_product.dart';
import 'package:digikala/pages/home/widgets/special_offers.dart';
import 'package:digikala/pages/home/widgets/super_market_offers.dart';
import 'package:digikala/pages/home/widgets/your_headphone_visits.dart';
import 'package:digikala/pages/home/widgets/your_mobile_visits.dart';
import 'package:digikala/pages/home/widgets/your_visits.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>  with SingleTickerProviderStateMixin{
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 4, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: MyColors.background,
        appBar: SearchBar(),
        bottomNavigationBar: BottomBar(_tabController),
        body: TabBarView(
          children: <Widget>[
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  const SizedBox(height: 24),
                  Banners(),
                  const SizedBox(height: 16),
                  Category(),
                  const SizedBox(height: 16),
                  SpecialOffers(),
                  const SizedBox(height: 16),
                  CategoryOffers(),
                  const SizedBox(height: 16),
                  SuperMarketOffers(),
                  const SizedBox(height: 16),
                  YourVisits(),
                  BannerOne(),
                  const SizedBox(height: 16),
                  YourMobileVisits(),
                  BannerTwo(),
                  BestSellingGoods(),
                  BannerThree(),
                  const SizedBox(height: 16),
                  YourHeadPhoneVisits(),
                  BestProductsDisplayed(),
                  const SizedBox(height: 16),
                  SelectedProduct(),
                  const SizedBox(height: 32),

                ],
              ),
            ),
            Container(child: Center(
              child: Text("در دست ساخت",style: TextStyle(
                color: MyColors.primary,fontSize: 18
              ),),
            ),),
            Container(child: Center(
              child: Text("در دست ساخت",style: TextStyle(
                color: MyColors.primary,fontSize: 18
              ),),
            ),),
            Container(child: Center(
              child: Text("در دست ساخت",style: TextStyle(
                color: MyColors.primary,fontSize: 18
              ),),
            ),),

          ],
          controller: _tabController,

        ),
      ),
    );
  }
}
