import 'package:digikala/configuration/colors.dart';
import 'package:flutter/material.dart';

class Chips extends StatelessWidget {
  final Color color;
  final String productColor;
  final bool state;

   Chips({@required this.color, @required this.productColor, this.state});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8,vertical: 4),
      margin: EdgeInsets.only(right: 16,top: 2,bottom: 8),
      decoration: BoxDecoration(
          color: MyColors.background,
          border: Border.all(color: state
              ? MyColors.catFour.withOpacity(.57)
           : MyColors.fontColor.withOpacity(.38)),
          borderRadius: BorderRadius.all(
            Radius.circular(100),
          ),
          boxShadow: [
            BoxShadow(
              color: state
          ? MyColors.catFour.withOpacity(.57)
            :MyColors.fontColor.withOpacity(.38),
              blurRadius: 2,
              spreadRadius: 0
            )
          ]),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            width: 22,
            height: 22,
            decoration:
                BoxDecoration(shape: BoxShape.circle, color: color,
                  border: Border.all(width: 1,color: MyColors.fontColor.withOpacity(.56))
                ),
            child: Center(
              child: state
                  ? Icon(Icons.check,color: MyColors.iconColor,size: 16,)
                  : null
            ),
          ),
          const SizedBox(width: 8),
          Text("$productColor"),
          const SizedBox(width: 8),


        ],
      ),
    );
  }

}
