import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/show_product/widgets/review_item_one.dart';
import 'package:digikala/pages/show_product/widgets/review_item_three.dart';
import 'package:digikala/pages/show_product/widgets/review_item_two.dart';
import 'package:digikala/pages/show_product/widgets/review_items.dart';
import 'package:digikala/pages/show_product/widgets/review_rate.dart';
import 'package:flutter/material.dart';

productReview(context) {
  final screenWidth = MediaQuery.of(context).size.width;
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      const SizedBox(height: 16),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Text(
          Strings.review,
          style: TextStyle(
            fontSize: 18,
            color: MyColors.blackColor.withOpacity(.87),
          ),
        ),
      ),
      const SizedBox(height: 16),
      Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        height: 86,
        child: Text(
          "هوآوی با معرفی رسمی سری گوشی‌های P30، نسخه Lite این محصول که در واقع نسبت به P30 و  P30 Pro ارزان‌قیمت‌تر است را به عنوان گوشی میان‌رده روانه بازار کرد. این محصول دارای رم 6 گیگابایتی و دوربین 48 مگاپیکسلی روانه بازار شده است. همانطور که از نام این گوشی مشخص است،P30 Lite برخی ویژگی‌های گوشی‌های بالارده P30 را در خود دارد طراحی ظاهری رضایت‌بخش این سری محصولات را به کاربر عرض کند. در عوض مشخصات سخت‌افزاری این گوشی را به محصولی متفاوت تبدیل کرده است. نمایشگری که با پنل­ LCD ساخته‌شده و فناوری به‌کاررفته در آن IPS است نشانی از مقرون‌به‌صرفه بودن این گوشی است. البته این نمایشگر رزولوشن بالایی دارد؛ به‌طوری‌که در اندازه‌ی 6.15اینچی‌اش، حدود 415 پیکسل را در هر اینچ جا داده است. مانند بیشتر گوشی‌های امروزی هوآوی، در پی 30 لایت هم نمایشگر تقریبا تمام قاب جلویی گوشی را پر کرده است. این مشخصه در کنار لبه­‌های براق قاب اصلی، جلوه­‌ای لوکس به هوآوی  P30 Liteبخشیده است. قاب پشتی هم تا حدی براق است. ویژگی دیگر مجهزشدن P30  به حسگر اثرانگشت است. این فناوری خطوط انگشت شما را با استفاده از فناوری جدید شناسایی می­‌کند و تنها با لمس انگشت شما قفل گوشی را باز می­‌کند. این حسگر در قاب پشتی P30 لایت جا خوش کرده است. نکته‌ی بعدی درباره‌ی این گوشی وجود سه دوربین در پشت گوشی است؛ سه دوربین که در یکی سنسور 48 مگاپیکسلی، در دومی سنسور 8 مگاپیکسلی و در سومی دوربین 2 مگاپیکسلی قرار گرفته است. دوربین سلفی هم به یک سنسور 24 مگاپیکسلی مجهز شده است که یادآور دیگر گوشی‌های سری P30 است. این دوربین‌ها تصاویر بی‌نقص و فوق‌العاده‌ی ثبت می‌کنند و انگار برای رقابت با گوشی‌های بالارده دیگر برندها ساخته شده است. قابلیت اتصال به شبکه­‌های 4G با سرعت بالا، بلوتوث نسخه­‌ی 4.2، نسخه­‌ی 9 از اندروید و حسگر تشخیص چهره اندروید از دیگر ویژگی­‌های جدید این گوشی هستند. از نظر سخت­‌افزاری هم این گوشی به تراشه­‌ی 12 نانومتری HiSilicon Kirin 710 مجهز شده است که در آن پردازنده‌­ای هشت‌هسته‌ا‌ی و قدرتمند قرارگرفته تا بتواند علاوه‌بر کارهای معمول، از قابلیت­‌های جدید که هوآوی این روزها روی آن تمرکز خاصی دارد، پشتیبانی کند. این نسخه از گوشی P30 Lite دارای 128 گیگابایت حافظه داخلی است که با 6 گیگابایت رم همراهی می‌شود."
                  .substring(0, 180) +
              "...",
          style:
              TextStyle(color: MyColors.fontColor, height: 1.5, fontSize: 14),
        ),
      ),
      const SizedBox(height: 16),

      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          children: <Widget>[
            Text(
              Strings.moreText,
              style: TextStyle(color: MyColors.catFour, fontSize: 14),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: MyColors.catFour,
              size: 14,
            )
          ],
        ),
      ),
      const SizedBox(height: 16),
      Container(
        width: screenWidth,
        height: 248,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            const SizedBox(width: 16),
            ReviewItems(child: reviewItemOne()),
            ReviewItems(child: reviewItemTwo()),
            ReviewItems(child: reviewItemThree()),

          ],
        ),
      ),
      SizedBox(
        height: 16,
      )
    ],
  );
}
