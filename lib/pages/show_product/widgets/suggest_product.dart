import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';

import 'package:digikala/pages/home/model/offers_model.dart';
import 'package:digikala/pages/home/widgets/show_product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

List<OffersModel> _list = [
  OffersModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "گوشی موبایب هواووی مدل p10 2010 دو سیم کارت با ظرفیت 32",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "گوشی موبایب هواووی مدل p10 2010 دو سیم کارت با ظرفیت 32",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/115081009.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "گوشی موبایب هواووی مدل p10 2010 دو سیم کارت با ظرفیت 32",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/112195068.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "گوشی موبایب هواووی مدل p10 2010 دو سیم کارت با ظرفیت 32",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/4079867.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "گوشی موبایب هواووی مدل p10 2010 دو سیم کارت با ظرفیت 32",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/1435840.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "گوشی موبایب هواووی مدل p10 2010 دو سیم کارت با ظرفیت 32",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/115099091.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "گوشی موبایب هواووی مدل p10 2010 دو سیم کارت با ظرفیت 32",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "گوشی موبایب هواووی مدل p10 2010 دو سیم کارت با ظرفیت 32",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
  OffersModel(
      imgUrl:
      "https://dkstatics-public.digikala.com/digikala-products/114233142.jpg?x-oss-process=image/resize,m_lfit,h_350,w_350/quality,q_60",
      text: "گوشی موبایب هواووی مدل p10 2010 دو سیم کارت با ظرفیت 32",
      cost: "3.265.000تومان",
      offer: "36%",
      time: "20:16:08",
      orginalCost: "300.000"),
];

leftBorder(index) {
  if(index< _list.length)
    return BorderSide(
        color: MyColors.fontColor.withOpacity(.56),
        width: .4
    );
}

suggestProducts() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      const SizedBox(height: 16),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Text("خریداران این محصول، این کالا را هم خریده اند",style: TextStyle(
            fontSize: 18,
            color: MyColors.blackColor.withOpacity(.87)
        ),),
      ),
      Container(
        height: 300,
        child: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: 24),
            scrollDirection: Axis.horizontal,
            itemCount: _list.length,
            itemBuilder: (context, index) {
              final items = _list[index];

              return GestureDetector(
                onTap: () => showProduct(context: context, img: items.imgUrl),
                child: Container(
                  width: 174,
                  height: 280,
                  margin: EdgeInsets.symmetric(horizontal: 4),
                  decoration: BoxDecoration(
                      color: MyColors.itemsColor,
                      border: Border(left: leftBorder(index))
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.network(
                        items.imgUrl,
                        height: 106,
                        width: 106,
                        fit: BoxFit.cover,
                      ),

                      Container(
                        height: 84,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Text(
                            items.text.length > 40
                                ? items.text.substring(0, 40) + "..."
                                : items.text,
                            textAlign: TextAlign.start,
                            textDirection: TextDirection.rtl,
                            style: TextStyle(
                                color: MyColors.fontColor,
                                fontSize: 14,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ),
                      const SizedBox(height: 16),

                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            items.cost,
                            style: TextStyle(
                                color: MyColors.blackColor,
                                fontWeight: FontWeight.bold,
                                fontSize: 14),
                            textAlign: TextAlign.end,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
      ),
    ],
  );
}
