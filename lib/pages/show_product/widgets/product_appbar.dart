
import 'package:digikala/configuration/colors.dart';
import 'package:digikala/pages/home/widgets/inkwel_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

productAppBar(context){
  return SliverAppBar(
    backgroundColor: MyColors.background,
    expandedHeight: 56,
    floating: true,
    pinned: false,
    leading: InkwellButton(icon: Icons.close,onTap: (){
      Navigator.pop(context);
    },),
    actions: <Widget>[
      InkwellButton(icon: Icons.shopping_basket,onTap: (){},),
      InkwellButton(icon: Feather.heart,onTap: (){},),
      InkwellButton(icon: Icons.more_vert,onTap: (){},),
    ],
  );
}