import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/show_product/widgets/review_item_two_widget.dart';
import 'package:flutter/cupertino.dart';

reviewItemTwo() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        Strings.plusPoint,
        style: TextStyle(fontSize: 16, color: MyColors.catTwo),
      ),
      const SizedBox(height: 8),
      reviewItemTwoWidget("قیمت پایین"),
      reviewItemTwoWidget("سیستم عامل اندروید "),
      reviewItemTwoWidget("نسبت تصویر 18:9"),
    ],
  );
}
