import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/show_product/widgets/review_item_three_widget.dart';
import 'package:flutter/cupertino.dart';

reviewItemThree() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        Strings.plusPoint,
        style: TextStyle(fontSize: 16, color: MyColors.primary),
      ),
      const SizedBox(height: 8),
      reviewItemThreeWidget("قیمت پایین"),
      reviewItemThreeWidget("سیستم عامل اندروید "),
      reviewItemThreeWidget("نسبت تصویر 18:9"),
    ],
  );
}
