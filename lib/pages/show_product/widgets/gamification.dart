import 'package:digikala/configuration/colors.dart';
import 'package:flutter/cupertino.dart';

gamification() {
  return Container(
    height: 120,
    color: MyColors.fontColor.withOpacity(.18),
    child: Container(
      margin: EdgeInsets.all(16),
      padding: EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        color: MyColors.background,
        borderRadius: BorderRadius.all(Radius.circular(6))
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.network(
            "https://www.digikala.com/static/files/e3b35801.png",
            width: 24,
            height: 24,
          ),
          const SizedBox(width: 16),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("با خرید این محصول شما",style: TextStyle(
                fontSize: 14,color: MyColors.fontColor.withOpacity(.57)
              ),),
              const SizedBox(height: 8),
              Text("99 امتیاز در دیجی کلاب دریافت می کنید",style: TextStyle(
                  fontSize: 16,color: MyColors.fontColor,fontWeight: FontWeight.w500,
              ),)
            ],
          )
        ],
      ),
    ),
  );
}
