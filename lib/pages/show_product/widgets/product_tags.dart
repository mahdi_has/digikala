import 'package:digikala/configuration/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

productTags({context, imgUrl}) {
  return Container(
    width: MediaQuery.of(context).size.width,
    height: 80,
    color: MyColors.fontColor.withOpacity(.18),
    child: ListView(
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        const SizedBox(width: 16),
        Chip(
          elevation: 2,
          backgroundColor: MyColors.background,
          label: Row(
            children: <Widget>[
              Image.network(
                imgUrl,
                width: 24,
                height: 24,
              ),
              const SizedBox(width: 4),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 6),
                child: Text("گوشی موبایل"),
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 16,
              )
            ],
          ),
        ),
        const SizedBox(width: 8,),
        Chip(
          elevation: 2,
          backgroundColor: MyColors.background,
          label: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 6),
                child: Text("موبایل"),
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 16,
              )
            ],
          ),
        ),
        const SizedBox(width: 8,),
        Chip(
          elevation: 2,
          backgroundColor: MyColors.background,
          label: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 6),
                child: Text("کالای دیجیتال"),
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 16,
              )
            ],
          ),
        ),
        const SizedBox(width: 8,),
        Chip(
          elevation: 2,
          backgroundColor: MyColors.background,
          label: Row(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 6),
                child: Text("کالای ارتباطی"),
              ),
              Icon(
                Icons.arrow_forward_ios,
                size: 16,
              )
            ],
          ),
        ),
      ],
    ),
  );
}
