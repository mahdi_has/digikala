

import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/show_product/widgets/review_rate.dart';
import 'package:flutter/material.dart';

reviewItemOne(){
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(
        Strings.point,
        style: TextStyle(fontSize: 16, color: MyColors.fontColor),
      ),
      const SizedBox(height: 8),

      Row(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Text(
              Strings.quality,
              overflow: TextOverflow.ellipsis,
              style:
              TextStyle(fontSize: 16, color: MyColors.fontColor),
            ),
          ),
          Expanded(
            flex: 5,
            child: reviewRate(3),
          ),
          const SizedBox(width: 8),
          Expanded(flex: 1,
            child: Text("3.0",style: TextStyle(
                fontSize: 12,color: MyColors.fontColor.withOpacity(.87)
            ),),)
        ],
      ),
      Row(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Text(
              Strings.byeValue,
              overflow: TextOverflow.ellipsis,

              style:
              TextStyle(fontSize: 16, color: MyColors.fontColor),
            ),
          ),
          Expanded(
            flex: 5,
            child: reviewRate(5),
          ),
          const SizedBox(width: 8),
          Expanded(flex: 1,
            child: Text("5.0",style: TextStyle(
                fontSize: 12,color: MyColors.fontColor.withOpacity(.87)
            ),),)
        ],
      ),
      Row(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Text(
              Strings.innovation,
              overflow: TextOverflow.ellipsis,

              style:
              TextStyle(fontSize: 16, color: MyColors.fontColor),
            ),
          ),
          Expanded(
            flex: 5,
            child: reviewRate(1),
          ),
          const SizedBox(width: 8),
          Expanded(flex: 1,
            child: Text("1.0",style: TextStyle(
                fontSize: 12,color: MyColors.fontColor.withOpacity(.87)
            ),),)
        ],
      ),
      Row(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Text(
              Strings.possibilities,
              overflow: TextOverflow.ellipsis,

              style:
              TextStyle(fontSize: 16, color: MyColors.fontColor),
            ),
          ),
          Expanded(
            flex: 5,
            child: reviewRate(3),
          ),
          const SizedBox(width: 8),
          Expanded(flex: 1,
            child: Text("3.0",style: TextStyle(
                fontSize: 12,color: MyColors.fontColor.withOpacity(.87)
            ),),)
        ],
      ),
      Row(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Text(
              Strings.easeOfUse,
              overflow: TextOverflow.ellipsis,

              style:
              TextStyle(fontSize: 16, color: MyColors.fontColor),
            ),
          ),
          Expanded(
            flex: 5,
            child: reviewRate(2),
          ),
          const SizedBox(width: 8),
          Expanded(flex: 1,
            child: Text("2.0",style: TextStyle(
                fontSize: 12,color: MyColors.fontColor.withOpacity(.87)
            ),),)
        ],
      ),
      Row(
        children: <Widget>[
          Expanded(
            flex: 4,
            child: Text(
              Strings.design,
              overflow: TextOverflow.ellipsis,

              style:
              TextStyle(fontSize: 16, color: MyColors.fontColor),
            ),
          ),
          Expanded(
            flex: 5,
            child: reviewRate(4),
          ),
          const SizedBox(width: 8),
          Expanded(flex: 1,
            child: Text("4.0",style: TextStyle(
                fontSize: 12,color: MyColors.fontColor.withOpacity(.87)
            ),),)
        ],
      ),

    ],
  );
}