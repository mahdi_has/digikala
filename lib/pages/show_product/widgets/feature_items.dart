import 'package:digikala/configuration/colors.dart';
import 'package:flutter/cupertino.dart';

featureItems({title , text}) {
  return Row(
    children: <Widget>[
      Container(
        margin: EdgeInsets.symmetric(vertical: 10),
        width: 10,
        height: 10,
        decoration: BoxDecoration(
            shape: BoxShape.circle, color: MyColors.fontColor.withOpacity(.38)),
      ),
      const SizedBox(width: 8),

      Text(title,style: TextStyle(
          color: MyColors.fontColor,
        fontSize: 16
      ),),
      const SizedBox(width: 8),
      Text(text,style: TextStyle(
          color: MyColors.blackColor.withOpacity(.87),
          fontSize: 14
      ),)
    ],
  );
}
