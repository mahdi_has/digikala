import 'package:digikala/configuration/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import 'chips.dart';

// ignore: must_be_immutable
class ProductTopPart extends StatefulWidget {
  String imgUrl;

  ProductTopPart({this.imgUrl});

  @override
  _ProductTopPartState createState() => _ProductTopPartState();
}

class _ProductTopPartState extends State<ProductTopPart> {
  bool state1 = true;
  bool state2 = false;
  bool state3 = false;
  String proColor;

  productColor() {
    if (state1) {
      return proColor = "مشکی";
    } else if (state2) {
      return proColor = "قرمز";
    } else
      return proColor = "سبز";
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    productColor();
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const SizedBox(height: 16),
        Image.network(
          widget.imgUrl,
          width: screenWidth * 16 / 9,
          height: screenHeight / 3,
        ),
        const SizedBox(height: 16),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            "سامسونگ",
            style: TextStyle(color: MyColors.catFour),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Text(
            "گوشی موبایل سامسونگ مدل A50 2020 دو سیم کارت ظرفیت 32 گیگابایت",
            style: TextStyle(
                color: MyColors.blackColor.withOpacity(.87),
                fontSize: 18,
                fontWeight: FontWeight.bold,
                height: 1.5),
          ),
        ),
        const SizedBox(height: 16),
        Padding(
          padding: const EdgeInsets.only(left: 22),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Text("(1022)",style: TextStyle(
                fontSize: 11,color: MyColors.fontColor
              ),),
              const SizedBox(width: 4),
              RatingBar(
                itemSize: 18,
                initialRating: 3.5,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                textDirection: TextDirection.ltr,
                itemPadding: EdgeInsets.symmetric(horizontal: 1),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 8),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Divider(
            color: MyColors.fontColor,
          ),
        ),
        const SizedBox(height: 8),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            "رنگ: $proColor",
            style: TextStyle(
                color: MyColors.blackColor.withOpacity(.87),
                fontSize: 18,
                height: 1.5),
          ),
        ),
        const SizedBox(height: 16),
        Container(
          height: 46,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  productColor();
                  print(state1);
                  setState(() {
                    state1 = true;
                    state2 = false;
                    state3 = false;
                  });
                },
                child: Chips(
                  state: state1,
                  color: MyColors.blackColor,
                  productColor: "مشکی",
                ),
              ),
              GestureDetector(
                onTap: () {
                  productColor();

                  print(state2);

                  setState(() {
                    state1 = false;
                    state2 = true;
                    state3 = false;
                  });
                },
                child: Chips(
                  state: state2,
                  color: MyColors.catOne,
                  productColor: "قرمز",
                ),
              ),
              GestureDetector(
                onTap: () {
                  productColor();

                  setState(() {
                    state1 = false;
                    state2 = false;
                    state3 = true;
                  });
                },
                child: Chips(
                  state: state3,
                  color: MyColors.catTwo,
                  productColor: "سبز",
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
