import 'package:digikala/configuration/colors.dart';
import 'package:flutter/material.dart';

myDivider(context){
  final screenWidth = MediaQuery.of(context).size.width;
  return Container(
    decoration: BoxDecoration(
        color: MyColors.fontColor.withOpacity(.18),
        border: Border.all(
          width: .2,
          color: MyColors.fontColor.withOpacity(.57),
        )),
    height: 12,
    width: screenWidth,
  );
}