import 'package:digikala/configuration/colors.dart';
import 'package:flutter/material.dart';

reviewItemTwoWidget(String text){
  return Container(
    margin: EdgeInsets.symmetric(vertical: 6),
    child: Row(
      children: <Widget>[
        Text(
          "+",
          style: TextStyle(fontSize: 16, color: MyColors.catTwo),
        ),
        const SizedBox(width: 16),
        Text(
          text,
          style: TextStyle(fontSize: 14, color: MyColors.fontColor),
        ),
      ],
    ),
  );
}