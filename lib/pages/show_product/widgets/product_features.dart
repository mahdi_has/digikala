import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/show_product/widgets/my_divider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'feature_items.dart';

productFeatures(context) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 16),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          Strings.productFeature,
          style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w500,
              color: MyColors.blackColor.withOpacity(.87)),
        ),
        const SizedBox(height: 16),
        featureItems(title: "شبکه های ارتباطی:", text: "4G, 3G, 2G"),
        featureItems(title: "سیستم عامل:", text: "Android"),
        featureItems(title: "مقدار RAM:", text: "1 گیگابایت"),
        featureItems(title: "رزولوشن عکس", text: "8 مگاپیکسل"),
        featureItems(title: "نسخه سیستم عامل", text: "Oreo  8.1"),
        featureItems(title: "اندازه", text: "5.45"),
        Divider(
          color: MyColors.fontColor,
        ),
        const SizedBox(height: 16),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.warning),
            const SizedBox(width: 16),
            Container(
              height: 120,
              width: MediaQuery.of(context).size.width-72,
              child: Text(
                Strings.warning,
                textAlign: TextAlign.right,
                textDirection: TextDirection.rtl,
                style: TextStyle(
                    height: 1.5, fontSize: 14, color: MyColors.fontColor),
              ),
            ),
          ],
        ),
        Divider(
          color: MyColors.fontColor,
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: 16),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 10,
                child: Text(
                  Strings.technicalSpecifications,
                  style: TextStyle(
                      color: MyColors.blackColor.withOpacity(.87),
                      fontSize: 16),
                ),
              ),
              Expanded(
                flex: 1,
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: 14,
                  color: MyColors.fontColor,
                ),
              )
            ],
          ),
        ),
        const SizedBox(height: 8),
      ],
    ),
  );
}
