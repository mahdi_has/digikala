import 'package:digikala/configuration/colors.dart';
import 'package:flutter/material.dart';


class CommentItems extends StatefulWidget {
  final Widget child;

  const CommentItems({Key key, this.child}) : super(key: key);
  @override
  _CommentItemsState createState() => _CommentItemsState();
}

class _CommentItemsState extends State<CommentItems> {
  bool tap = false;

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    return InkWell(
      onHighlightChanged: (event){
        print(event);
        if(event)
          setState(() {
            tap = true;
          });
        else
          setState(() {
            tap = false;
          });
      },
      onTap: (){},
      splashColor: Colors.white.withOpacity(.1),
      highlightColor: Colors.white.withOpacity(.1),
      customBorder: ShapeBorder.lerp(
          RoundedRectangleBorder(
              side: BorderSide(color: Colors.white, width: 1)),
          RoundedRectangleBorder(
              side: BorderSide(color: Colors.white, width: 1)),
          1),
      child: Stack(
        children: <Widget>[

          Container(
            margin: const EdgeInsets.only(left: 16, top: 4, bottom: 4),
            padding: EdgeInsets.all(16),
            width: screenWidth - 64,
            height: 240,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(6)),
                color: MyColors.background,
                boxShadow: [
                  tap ? BoxShadow(
                      color: MyColors.fontColor.withOpacity(.1),
                      blurRadius: 2)

                      : BoxShadow(
                      color: MyColors.fontColor.withOpacity(.57),
                      blurRadius: 2)
                ]),
            child: widget.child,
          ),
          Visibility(
            visible: tap,
            child: Container(
              margin: const EdgeInsets.only(left: 16, top: 4, bottom: 4),
              padding: EdgeInsets.all(16),
              width: screenWidth - 64,
              height: 240,
              color: MyColors.itemsColor.withOpacity(.38),

            ),
          ),
        ],
      ),
    );
  }
}
