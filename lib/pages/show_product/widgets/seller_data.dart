import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

sellerData() {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 24),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          Strings.seller,
          style: TextStyle(
              fontSize: 18, color: MyColors.blackColor.withOpacity(.87)),
        ),
        const SizedBox(height: 16),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Center(
                child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        bottomRight: Radius.circular(6),
                        bottomLeft: Radius.circular(6),
                        topRight: Radius.circular(6)),
                    child: Image.asset(
                      "assets/images/digikala.png",
                      width: 26,
                      height: 20,
                      fit: BoxFit.fitWidth,
                    )),
              ),
            ),
            const SizedBox(width: 16),
            Expanded(
              flex: 8,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    Strings.digikala,
                    style: TextStyle(fontSize: 18, color: MyColors.fontColor),
                  ),
                  const SizedBox(height: 8),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.star,
                        size: 18,
                        color: MyColors.fontColor.withOpacity(.56),
                      ),
                      const SizedBox(width: 4),
                      Text(
                        "5.0",
                        style: TextStyle(
                            fontSize: 16,
                            color: MyColors.fontColor.withOpacity(.56)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Expanded(
                flex: 1,
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: 16,
                  color: MyColors.fontColor.withOpacity(.56),
                ))
          ],
        ),
        const SizedBox(height: 8),
        Divider(
          color: MyColors.fontColor,
          indent: 36,
        ),
        const SizedBox(height: 8),
        Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Icon(Feather.check_square),
            ),
            const SizedBox(width: 16),
            Expanded(
              flex: 10,
              child: Text(
                "گارانتی اصلات وسلامت فیزیکی کالا",
                style: TextStyle(fontSize: 18),
              ),
            )
          ],
        ),
        const SizedBox(height: 8),
        Divider(
          color: MyColors.fontColor,
          indent: 36,
        ),
        const SizedBox(height: 8),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Center(
                child: Image.asset(
                  "assets/images/send.png",
                  width: 36,
                  height: 22,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
            const SizedBox(width: 16),
            Expanded(
              flex: 10,
              child: Text(
                Strings.sendText,
                style: TextStyle(fontSize: 16),
              ),
            )
          ],
        ),
        const SizedBox(height: 16),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text(
              "آیا قیمت مناسب تری سراغ دارید؟",
              style: TextStyle(color: MyColors.fontColor, fontSize: 12),
            ),
            const SizedBox(width: 8),
            Transform.rotate(
                angle: 1.4,
                child: Icon(
                  Icons.local_offer,
                  size: 16,
                  color: MyColors.fontColor,
                ))
          ],
        )
      ],
    ),
  );
}
