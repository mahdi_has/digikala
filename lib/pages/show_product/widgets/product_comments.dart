import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/show_product/widgets/comment_items.dart';
import 'package:flutter/material.dart';

productComments(context) {
  final screenWidth = MediaQuery.of(context).size.width;
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      const SizedBox(height: 16),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              Strings.comments,
              style: TextStyle(
                fontSize: 18,
                color: MyColors.blackColor.withOpacity(.87),
              ),
            ),
            Text(
              "56 نظر",
              style: TextStyle(
                fontSize: 14,
                color: MyColors.catFour,
              ),
            ),
          ],
        ),
      ),
      const SizedBox(height: 16),
      Container(
        width: screenWidth,
        height: 248,
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            const SizedBox(width: 16),
            CommentItems(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "خیلی ممنون",
                  style: TextStyle(fontSize: 16, color: MyColors.fontColor),
                ),
                const SizedBox(height: 16),
                Text(
                  "خرید این محصول را توصیه می کنم",
                  style: TextStyle(fontSize: 14, color: MyColors.catTwo),
                ),
                const SizedBox(height: 16),
                Text(
                  "من این گوشی رو برای یکی از دوستان خریدمو از خریدش راضی هست گوشی بدی نبود و باتوجه به قیمت انتخاب خوبی بود",
                  style: TextStyle(fontSize: 16, height: 1.8),
                ),
              ],
            )),
            CommentItems(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "خیلی ممنون",
                  style: TextStyle(fontSize: 16, color: MyColors.fontColor),
                ),
                const SizedBox(height: 16),
                Text(
                  "خرید این محصول را توصیه می کنم",
                  style: TextStyle(fontSize: 14, color: MyColors.catTwo),
                ),
                const SizedBox(height: 16),
                Text(
                  "من این گوشی رو برای یکی از دوستان خریدمو از خریدش راضی هست گوشی بدی نبود و باتوجه به قیمت انتخاب خوبی بود",
                  style: TextStyle(fontSize: 16, height: 1.8),
                ),
              ],
            )),
            CommentItems(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "خیلی ممنون",
                  style: TextStyle(fontSize: 16, color: MyColors.fontColor),
                ),
                const SizedBox(height: 16),
                Text(
                  "خرید این محصول را توصیه می کنم",
                  style: TextStyle(fontSize: 14, color: MyColors.catTwo),
                ),
                const SizedBox(height: 16),
                Text(
                  "من این گوشی رو برای یکی از دوستان خریدمو از خریدش راضی هست گوشی بدی نبود و باتوجه به قیمت انتخاب خوبی بود",
                  style: TextStyle(fontSize: 16, height: 1.8),
                ),
              ],
            )),
          ],
        ),
      ),
      SizedBox(
        height: 16,
      ),
      Divider(
        color: MyColors.fontColor,
        indent: 16,
        endIndent: 16,
      ),
      SizedBox(
        height: 16,
      ),
      Row(
        children: <Widget>[
          SizedBox(width: 16),
          Icon(
            Icons.check_circle,
            color: MyColors.primary,
            size: 20,
          ),
          const SizedBox(width: 4),
          Container(
            width: screenWidth - 48,
            child: Text(
              "بیش از 540 نفر از خریداران این محصول را پیشنهاد داده اند",
              style: TextStyle(fontSize: 16, color: MyColors.fontColor),
            ),
          ),
        ],
      ),
      SizedBox(
        height: 16,
      ),
      Divider(
        color: MyColors.fontColor,
        indent: 16,
        endIndent: 16,
      ),
      SizedBox(
        height: 16,
      ),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 10,
              child: Text("پرسش و پاسخ",style: TextStyle(
                fontSize: 18,color: MyColors.blackColor.withOpacity(.87)
              ),),
            ),
            Expanded(flex: 1,
            child: Icon(Icons.arrow_forward_ios,size: 14,color: MyColors.fontColor,),)
          ],
        ),
      ),
      SizedBox(
        height: 16,
      ),

    ],
  );
}
