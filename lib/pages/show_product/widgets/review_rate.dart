import 'package:digikala/configuration/colors.dart';
import 'package:flutter/cupertino.dart';

point(rate) {
  if(rate == 1){
    return Flex(
      mainAxisSize: MainAxisSize.min,
      direction: Axis.horizontal,
      children: <Widget>[
        Expanded(
          flex: 2,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                color: MyColors.catFour),
          ),
        ),
        Expanded(
          flex: 8,
          child: Container(),
        )

      ],
    );
  }
  else if(rate == 2){
    return Flex(
      mainAxisSize: MainAxisSize.min,
      direction: Axis.horizontal,
      children: <Widget>[
        Expanded(
          flex: 4,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                color: MyColors.catFour),
          ),
        ),
        Expanded(
          flex: 6,
          child: Container(),
        )

      ],
    );
  }
  else if(rate == 3){
    return Flex(
      mainAxisSize: MainAxisSize.min,
      direction: Axis.horizontal,
      children: <Widget>[
        Expanded(
          flex: 6,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                color: MyColors.catFour),
          ),
        ),
        Expanded(
          flex: 4,
          child: Container(),
        )

      ],
    );
  }
  else if(rate == 4){
    return Flex(
      mainAxisSize: MainAxisSize.min,
      direction: Axis.horizontal,
      children: <Widget>[
        Expanded(
          flex: 8,
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                color: MyColors.catFour),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(),
        )

      ],
    );
  }
  else if(rate == 5){
    return Flex(
      mainAxisSize: MainAxisSize.min,
      direction: Axis.horizontal,
      children: <Widget>[
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(16)),
                color: MyColors.catFour),
          ),
        ),


      ],
    );
  }
}

reviewRate(int rate) {
  return Container(
    width: 200,
    height: 12,
    margin: EdgeInsets.symmetric(vertical: 8),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(16)),
        color: MyColors.fontColor.withOpacity(.18)),
    child: point(rate)
  );
}
