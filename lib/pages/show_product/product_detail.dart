import 'package:digikala/configuration/colors.dart';
import 'package:digikala/configuration/strings.dart';
import 'package:digikala/pages/home/widgets/inkwel_icon.dart';
import 'package:digikala/pages/show_product/widgets/chips.dart';
import 'package:digikala/pages/show_product/widgets/gamification.dart';
import 'package:digikala/pages/show_product/widgets/my_divider.dart';
import 'package:digikala/pages/show_product/widgets/productReview.dart';
import 'package:digikala/pages/show_product/widgets/product_appbar.dart';
import 'package:digikala/pages/show_product/widgets/product_comments.dart';
import 'package:digikala/pages/show_product/widgets/product_features.dart';
import 'package:digikala/pages/show_product/widgets/product_tags.dart';
import 'package:digikala/pages/show_product/widgets/product_top_part.dart';
import 'package:digikala/pages/show_product/widgets/seller_data.dart';
import 'package:digikala/pages/show_product/widgets/similar_products.dart';
import 'package:digikala/pages/show_product/widgets/suggest_product.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vector_icons/flutter_vector_icons.dart';

class ProductDetail extends StatefulWidget {
  final String imgUrl;

  const ProductDetail({Key key, this.imgUrl}) : super(key: key);

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        bottomNavigationBar: BottomAppBar(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 16),
            height: 70,
            child: Flex(
              direction: Axis.horizontal,
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: Center(
                    child: Container(
                      decoration: BoxDecoration(
                        color: MyColors.primary,
                        borderRadius: BorderRadius.all(Radius.circular(6)),
                      ),
                      height: 46,
                      child: Center(
                        child: Text(
                          Strings.addToBasket,
                          style: TextStyle(
                              color: MyColors.iconColor, fontSize: 16),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "986,000 تومان",
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: MyColors.blackColor.withOpacity(.87)),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        backgroundColor: MyColors.background,
        body: SafeArea(
          child: NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[productAppBar(context)];
              },
              body: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    ProductTopPart(
                      imgUrl: widget.imgUrl,
                    ),
                    const SizedBox(height: 16),
                    myDivider(context),
                    sellerData(),
                    gamification(),
                    const SizedBox(height: 16),
                    similarProducts(),
                    myDivider(context),
                    const SizedBox(height: 16),
                    productFeatures(context),
                    myDivider(context),
                    productReview(context),
                    productTags(context: context, imgUrl: widget.imgUrl),
                    const SizedBox(height: 16),
                    productComments(context),
                    myDivider(context),
                    suggestProducts(),
                    myDivider(context),
                  ],
                ),
              )),
        ),
      ),
    );
  }
}
