class Strings {
  //category
  static String categoryTxtOne = "فروش ویژه";
  static String categoryTxtTwo = "سوپر مارکت";
  static String categoryTxtThree = "مد و پوشاک";
  static String categoryTxtFour = "کالای دیجیتال";
  static String categoryTxtFive = "خانه و آشپزخانه";

  //base
  static String seeAll = "مشاهده همه";

  //supermarket offer
  static String send = "ارسال سریع سوپرمارکتی";

//your visits
  static String yourVisitsOne = "ساعت هوشمند";
  static String sortBy = "بر اساس بازدید شما";
  static String seenAllVisits = "مشاهده بیش از 500 کالا";
  static String seenAllMobile = "مشاهده بیش از 2500 کالا";
  static String seenAllHeadphone = "مشاهده بیش از 230 کالا";
  static String yourVisitsTwo = "گوشی موبایل";
  static String yourVisitsThree = "هدفون، هدست، هندزفری";

  //best selling
  static String bestSelling = "پر فروشترین کالاها";

  //best product displayed
  static String mostRecentProducts = "محصولات پربازدید اخیر";

  //selected product
  static String selectedProductTitle = "منتخب محصولات تخفیف و حراج";

  static String seller = "فروشنده";
  static String digikala = "دیجی کالا";
  static String sendText = "آماده ارسال";
  static String similarProuct = "محصولات مشابه";
  static String addToBasket = "افزودن به سبد خرید";
  static String productFeature = "ویژگی های محصول";
  static String warning =
      "هشدار سامانه همتا: حتما در زمان تحویل دستگاه، به کمک کد فعال‌سازی چاپ شده روی جعبه یا کارت گارانتی، دستگاه را از طریق #7777*، برای سیم‌کارت خود فعال‌سازی کنید. آموزش تصویری در آدرس اینترنتی hmti.ir/05";
  static String technicalSpecifications = "مشخصات فنی";
  static String review = "نقد و بررسی";
  static String moreText ="ادامه مطلب";
  static String point = "امتیاز کلی";
  static String plusPoint = "نکات مثبت";
  static String quality = "کیفیت ساخت";
  static String byeValue = "ارزش خرید به نسبت قیمت";
  static String innovation = "نوآوری";
  static String possibilities = "امکانات و قابلیت ها";
  static String easeOfUse = "سهولت استفاده";
  static String design = "طراحی و دیزاین";

  static String comments = "نظرات کاربران";


}
