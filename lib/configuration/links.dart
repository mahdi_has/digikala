class Links {
  //banners
  static String banner1 =
      'https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020696.jpg';
  static String banner2 =
      'https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020647.jpg';
  static String banner3 =
      'https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020572.jpg';
  static String banner4 =
      'https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020702.jpg';
  static String banner5 =
      'https://dkstatics-public.digikala.com/digikala-adservice-banners/1000019287.jpg';

  //category offers
  static String catOfferOne =
      "https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020723.jpg";
  static String catOfferTwo =
      "https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020717.jpg";
  static String catOfferThree =
      "https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020750.jpg";
  static String catOfferFour =
      "https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020319.jpg";

//special offer
  static String specialOffer =
      "https://www.digikala.com/static/files/d9b15d68.png";

//supermarket offer
  static String supermarketOffer =
      "https://www.digikala.com/static/files/8af90c4b.png";



  //advertisement banners
    static String advertisementBannerOne = "https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020767.jpg";
    static String advertisementBannerTwo = "https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020313.jpg";
    static String advertisementBannerThree = "https://dkstatics-public.digikala.com/digikala-adservice-banners/1000020749.jpg";
}
