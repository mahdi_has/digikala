import 'package:flutter/material.dart';

class MyColors{

  static Color primary = Color(0xffEF394E);
  static Color baseColor = Color(0xfff1f3f4);
  static Color catOne = Color(0xffF12946);
  static Color catTwo = Color(0xff82C449);
  static Color catFour = Color(0xff00BFD6);
  static Color catFive = Color(0xffFFD700);
  static Color catThree = Color(0xff821553);
  static Color fontColor = Color(0xff505A50);
  static Color iconColor = Color(0xffffffff);
  static Color itemsColor = Color(0xffffffff);
  static Color blackColor = Color(0xff0000000);
  static Color itemOne = Color(0xffE3E8FE);
  static Color itemTwo = Color(0xffFFEDD9);
  static Color itemThree = Color(0xffFFE6EC);
  static Color itemFour = Color(0xffFEEDFD);
  static Color background = Color(0xffFFFFFF);
}