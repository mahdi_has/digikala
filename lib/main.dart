import 'package:digikala/pages/home/view.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Digikala',
      theme: ThemeData(
       accentColor: Colors.white,
        fontFamily: 'Yekan'
      ),
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
